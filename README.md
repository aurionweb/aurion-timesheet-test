# Aurion Timesheet Test

This test will help us to gauge your approaches to code design, implementation,
and quality. You'll build a simple timesheeting application—the specification
follows—in NodeJS on the server, and your choice of client-side technologies.

The scope of the application is small, but broad. The goal of this task is to
give you the opportunity to show off what you're best at, not to have you spend
long hours creating a polished prototype (unless, of course, polished prototypes
is what you want to show off).

## Specification

The following screenshot describes a minimum features set for the application.
You may expand this to a set of user stories and/or specs as you choose (this
developer is partial to Gherkin and cucumber.js).

![Example UI](https://raw.githubusercontent.com/howlingeverett/aurion-timesheet-test/master/ui.png)

You should demonstrate full-stack development capabilities by implementing the
feature set using both the server (NodeJS with whatever packages you prefer) and
the client. The exact technologies and the balance between client and server is
up to you.

### User Interface

The bare-bones user interface screenshot above is simply to demonstrate the
minimum feature set for your implementation. You may iterate or enhance this UI
as you choose.

### Model Layer

The application should implement persistence, but the nature or performance of
persistence isn't important so much as your server's API to it. Simple file or
even server session persistence is fine.

## Getting Started

To get started with this test, fork this repository to your personal Github,
and clone it locally. The test itself is a Node package, and includes some
suggested tooling: Babel on the server and client, and Webpack for bundling
client-side dependencies. Some basic setup and configuration is included,
but you are free to expand and change this tooling as you prefer.

To build the server and client-side assets after installing your dependencies,
run `npm run-script build`.

## Submitting

To submit your work, simply create a pull request against the master repo. You
should also email your recruitment contact since, while the developers will be
monitoring the repository for PRs, your recruitment contact may not have access.
