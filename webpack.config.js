"use strict";

const path = require("path");

const webpack = require("webpack");

module.exports = {

  context: path.resolve(__dirname, "./client"),

  entry: {
    "app": "./index"
  },

  output: {
    filename: "[name].js",
    path: path.resolve(__dirname, "./dist/client")
  },

  module: {

    loaders: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        loader: "babel",
        query: {
          presets: ["es2015-webpack"]
        },
        cacheDirectory: true
      }
    ]
  },

  plugins: [
    new webpack.NoErrorsPlugin()
  ],

  devtool: "source-map"
};
