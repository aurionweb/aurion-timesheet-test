/**
 * Entrypoint for the client, mostly to enable the Webpack configuration
 */
import "babel-polyfill";
